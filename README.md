# Dotfiles

![dotfiles](image.png)

**Dotfiles** are stored with full path (e.g. `~/.config/nvim/init.vim`) and referenced in a *bash array*. When (*sym*)linking files, folders are created if necessary using `mkdir -p`. 

**Dotfiles** are stored without the *dot* for simplifying browsing and links are of only two types: a file or a folder. No combination of `ln -sfFnh…` managed to replace an exiting directory with a symlink so the target folder is removed before the link is made.

Adding or removing **dotfiles** is a manual process involving moving file to **dotfiles** folder, adding to array and then linking to `$HOME` but the frequency of doing so is seldom. Old files that are no longer in use can therefore be stored in the **dotfiles** folder if one desires. 
