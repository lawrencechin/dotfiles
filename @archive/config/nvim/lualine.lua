--[[
local my_theme = require( "lualine.themes.auto" )
-- my_theme.normal.c.bg = my_theme.normal.a.bg
-- my_theme.insert.c.bg = my_theme.insert.a.bg
-- my_theme.replace.c.bg = my_theme.replace.a.bg
-- my_theme.visual.c.bg = my_theme.visual.a.bg
-- my_theme.command.c.bg = my_theme.command.a.bg
my_theme.normal.c.fg = my_theme.normal.a.fg
my_theme.insert.c.fg = my_theme.insert.a.fg
my_theme.replace.c.fg = my_theme.replace.a.fg
my_theme.visual.c.fg = my_theme.visual.a.fg
my_theme.command.c.fg = my_theme.command.a.fg

local colors = {
    color1 = "#006262",
    color2 = "#008665",
    color3 = "#00AA89",
    color4 = "#83AA36",
    color5 = "#651210",
    color6 = "#8A1813",
    color7 = "#886263",
    color8 = "#D26125",
    color9 = "#F68530"
}

local cobalt2 = require( "lualine.themes.auto" )
cobalt2.visual.c.bg = colors.color1
cobalt2.inactive.c.bg = colors.color1
cobalt2.insert.c.bg = colors.color1
cobalt2.replace.c.bg = colors.color1
cobalt2.normal.c.bg = colors.color1
cobalt2.command.c.bg = colors.color1

local inactive = { color = "StatusLineNC" }

require'lualine'.setup {
    options = {
        icons_enabled = false,
        theme = "tokyonight",
        component_separators = {'', ''},
        section_separators = {'', ''},
        disabled_filetypes = {}
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = {},
        lualine_c = {'filename' },
        lualine_x = {'g:coc_status', 'bo:filetype'},
        lualine_y = {'progress'},
        lualine_z = {'location'}
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {'filename'},
        lualine_x = {'location'},
        lualine_y = {},
        lualine_z = {}
    },
    tabline = {},
    extensions = {}
}
]]--
