" A pretty good primer on this stuff: http://dougblack.io/words/a-good-vimrc.html

" Remapping keys
" Set the user leader key to space, default '\'
let mapleader="\<Space>" 

" remap esc to jk/jj in insert mode
" using ESC that has been mapped to caps lock
" inoremap jj <esc> 
nmap <leader>ne :NERDTreeToggle<cr>
" Remap vim's search to use 'normal' regex
nnoremap / /\v
vnoremap / /\v
" Map . in visual mode to repeat action over all selected lines
vnoremap . :norm.<CR>

" Change split navigation
noremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Remove buffer without changing current pane-split arrangement  
nnoremap <leader>bd :bp<cr>:bd #<cr>

" Shortcut Command for changing pwd for current open file
command! CDC cd %:p:h

" COC Recommended Settings

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always display signcolumns (left sided space for error symbols)
set signcolumn=yes

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Try these lines to speed up the annoying lag witnessed by vim
set nocursorline    " don't paint cursor line
set nocursorcolumn  " don't paint cursor column
set nolazyredraw      " don't wait to redraw
set noshowcmd

syntax on " Enable syntax highlighting
" set number " Enable relative line numbering
" set encoding=utf-8
set hidden " Unsaved buffers will be hidden rather than closed
set spell spelllang=en,es " Turn on spell checking and set to english and españa

if has("macunix")
    set cb+=unnamed " Allows copying to system clipboard in OSX
endif

" Searching
set hlsearch " Search as characters are entered
set incsearch " Highlight matches

" Backups - all sent to /tmp
set backup
set undofile
set backupcopy=yes
set backupdir=/var/tmp//,/tmp//,/private/tmp//
set undodir=/var/tmp//,/tmp//,/private/tmp//
set directory=/var/tmp//,/tmp//,/private/tmp//

" Clear Registers
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor

" Set Python3 path for shorter startup time
let g:python3_host_prog='/Users/$USER/.penv/venv/bin/python3'
let g:python3_host_skip_check=1

" Download and install VimPlug if it doesn't exist
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')

" PlugInstall to install new plugins
" PlugClean to remove plugins previously installed but not listed here

" Add or remove you plugins here : 
Plug 'vim-airline/vim-airline' " Status bar
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' } " Sidebar
Plug 'vim-airline/vim-airline-themes' " Themes for airline
Plug 'othree/html5.vim' " HTML5 syntax
Plug 'pangloss/vim-javascript' " js syntax
Plug 'hail2u/vim-css3-syntax' " CSS3 highlights
Plug 'mxw/vim-jsx' " JSX syntax highlighting
Plug 'dag/vim-fish' " Vim support for editing fish scripts
Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' } " Python syntax highlighting
Plug 'neoclide/coc.nvim', { 'branch': 'release' } " Intellisense engine

" COLOUR SCHEMES
Plug 'challenger-deep-theme/vim', { 'as': 'challenger-deep' }

call plug#end()

" Required:
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

" CocExtensions

let g:coc_global_extensions = ['coc-python', 'coc-eslint', 'coc-tsserver', 'coc-html', 'coc-fish', 'coc-json', 'coc-css']

" Autocmd 
augroup markdown
    autocmd!
    autocmd BufRead,BufNewFile *.txt,*.TXT set filetype=markdown " Set markdown syntax to be used with txt files
augroup END

augroup terminal_open
    autocmd!
    autocmd TermOpen * setlocal nospell | setlocal wrap
augroup END

" Plugin settings

" Colors! Moved to the bottom so that the colorscheme works
" with the plugin version
" Airline Themes
" Light - lucius, biogoo, qwq
" Dark - night_owl, peaksea
" Both - kolor, aurora, cobalt2, fruit_punch, silver, dracula, fairyfloss, soda
"
let g:airline_theme='cobalt2'
let g:airline#extensions#coc#enabled = 1 " Enable COC in airline
colorscheme delek_extended

" VimR options
if has("gui_vimr")
    set termguicolors " enable true colour support
    let g:airline_theme='fairyfloss'

    " Use NVR for git commits within neo vim terminal
    if has("nvim")
        let $GIT_EDITOR = 'nvr -cc split --remote-wait'
    endif
endif

" Allows syntax highlighting in fenced code blocks, add more languages as you feel the need
let g:markdown_fenced_languages=['html', 'javascript', 'css', 'json=javascript', 'ruby', 'python', 'vim', 'sass', 'sh', 'zsh=sh', 'bash=sh'] 
