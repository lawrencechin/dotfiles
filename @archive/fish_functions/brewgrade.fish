function brewgrade --description 'Run modBrew, upgrade and cleanup'
	modBrew;
    brew upgrade;
    brew cleanup --prune 0;
end
