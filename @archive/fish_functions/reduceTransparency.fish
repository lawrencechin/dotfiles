function reduceTransparency --description 'toggle reduce transparency on and off'
	set currentStatus (defaults read com.apple.universalaccess reduceTransparency)
if test "$currentStatus" -eq 0
defaults write com.apple.universalaccess reduceTransparency -bool true
else
defaults write com.apple.universalaccess reduceTransparency -bool false
end

killall Finder
killall Spotlight
end
