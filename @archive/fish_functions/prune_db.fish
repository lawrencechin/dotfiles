function prune_db --description "Vacuum and backup current photos database"
    $UTILS/prune_photos_db.sh
end
