function modBrew --description 'Reapply an homebrew edit after an update'
	cd /usr/local/Homebrew && git stash pop && cd -
end
