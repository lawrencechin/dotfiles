switcher = hs.window.switcher.new()

hs.window.switcher.ui.showSelectedThumbnail = false
hs.window.switcher.ui.textSize = 12
hs.window.switcher.ui.backgroundColor = { 0.3, 0.3, 0.3, 0.95 }
hs.window.switcher.ui.highlightColor = { 0.9, 0.9, 0.9, 0.4 }
hs.window.switcher.ui.titleBackgroundColor = { 0.15, 0.15, 0.15, 0.8 }
hs.window.switcher.ui.textColor = { 1, 1, 1 }
hs.window.switcher.ui.thumbnailSize = 180
return switcher
