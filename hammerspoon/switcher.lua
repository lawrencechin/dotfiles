local obj = {}
obj.__index = obj

theWindows = hs.window.filter.new()
theWindows:setDefaultFilter{}
theWindows:setSortOrder( hs.window.filter.sortByFocusedLast )
obj.currentWindows = {}

-- Start by saving all windows
for i,v in ipairs( theWindows:getWindows()) do
    table.insert( obj.currentWindows, v )
end

-- the hammerspoon tracking of windows seems to be broken
-- we do it ourselves
local function callback_window_created( w, appName, event )
    if event == "windowDestroyed" then
        for i,v in ipairs( obj.currentWindows ) do
            if v == w then
                table.remove( obj.currentWindows, i )
                return
            end
        end
        return
    end

    if event == "windowCreated" then
        table.insert( obj.currentWindows, 1, w )
        return
    end

    if event == "windowFocused" then
        callback_window_created( w, appName, "windowDestroyed" )
        callback_window_created( w, appName, "windowCreated" )
    end
end

theWindows:subscribe( hs.window.filter.windowCreated, callback_window_created )
theWindows:subscribe( hs.window.filter.windowDestroyed, callback_window_created )
theWindows:subscribe( hs.window.filter.windowFocused, callback_window_created )


function obj:list_window_choices( onlyCurrentApp )
   local windowChoices = {}
   local currentWin = hs.window.focusedWindow()
   local currentApp = currentWin:application()

   for i,w in ipairs( obj.currentWindows ) do
      if w ~= currentWin then
         local app = w:application()
         local appName = app and app:name() or '(none)'

         if ( not onlyCurrentApp ) or ( app == currentApp ) then
             table.insert( windowChoices, {
                 subText = appName,
                 uuid = i,
                 win = w
             })
         end
      end
   end

   return windowChoices;
end

function obj:switchWindow( onlyCurrentApp )
    local windowChoices = obj:list_window_choices( onlyCurrentApp )
    if #windowChoices == 0 then
        return
    end

    local c = #windowChoices
    local v = windowChoices[ c ][ "win" ]

    if v then
        v:focus()
    end
end

-- cycles through all widows of the frontmost app.
function obj:cycleWindows()
    obj:switchWindow( true )
end

return obj
