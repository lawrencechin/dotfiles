function set_wallpaper_db()
    local home = os.getenv( "HOME" )
    local wallpaper = hs.execute( 'sqlite3 ~/.local/share/wallpapers.db \'.separator ""\' \'SELECT Path.root, Wallpapers.partial_path, Wallpapers.name FROM Wallpapers, Path, Selected WHERE Wallpapers.id = Selected.wallpaper_id;\'' )
    wallpaper = home .. wallpaper:match( "(.-)%s*$" )
    hs.osascript.applescript( 'tell application "Finder" to set desktop picture to POSIX file "' .. wallpaper .. '"' )
end

function set_wallpaper( state )
    local wallpaper_script_path = os.getenv( "HOME" ) .. "/.local/share/"
    local light_script = "light_wallpaper_random.applescript"
    local dark_script = "dark_wallpaper_random.applescript"
    if( state ) then
        hs.osascript.applescriptFromFile( wallpaper_script_path .. dark_script )
    else
        hs.osascript.applescriptFromFile( wallpaper_script_path .. light_script )
    end
end 

local module = {
    set_wallpaper = set_wallpaper
}

return module
