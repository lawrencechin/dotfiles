local obj = {}

local function positionWindow( win, widthFrac, heightFrac, left, top )
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x + ( max.w * left )
    f.y = max.y + ( max.h * top )
    f.w = max.w * widthFrac
    f.h = max.h * heightFrac
    win:setFrame( f )
end

local function moveWindow( win, left, right, top, bottom, nudge )
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()
    if( nudge ) then
        f.x = left and ( f.x - 15 ) or right and ( f.x + 15 ) or f.x
        f.y = top and ( f.y - 15 ) or bottom and ( f.y + 15 ) or f.y
    else
        f.x = left and 0 or right and ( max.w - f.w ) or f.x
        f.y = top and 0 or bottom and ( max.h - f.h + 25 ) or f.y
    end

    win:setFrame( f )
end

local function resizeWindow( win, grow, horizontal )
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()
    if( horizontal ) then
        f.w = grow and ( f.w + 15 ) or ( f.w - 15 )
    else
        f.h = grow and ( f.h + 15 ) or ( f.h - 15 )
    end
    win:setFrame( f )
end

function obj:shrink_horizontal()
    resizeWindow( hs.window.focusedWindow(), false, true )
end

function obj:shrink_vertical()
    resizeWindow( hs.window.focusedWindow(), false, false )
end

function obj:grow_horizontal()
    resizeWindow( hs.window.focusedWindow(), true, true )
end

function obj:grow_vertical()
    resizeWindow( hs.window.focusedWindow(), true, false )
end

function obj:default_safari_size()
    local win = hs.window.focusedWindow()
    local app = win:application():name()
    if ( app == "Safari" or app == "Brave Browser" or app == "Firefox" ) then
        local f = win:frame()
        --f.w = 1105
        --f.h = 710
        f.w = 1240
        f.h = 925
        win:setFrame( f )
        obj:centre()
    end
end

function obj:default_finder_size()
    local win = hs.window.focusedWindow()
    local app = win:application():name()
    if ( app == "Finder" ) then
        local f = win:frame()
        f.w = 550
        f.h = 600
        win:setFrame( f )
    end
end

function obj:left()
  positionWindow(hs.window.focusedWindow(), 1/2, 1, 0, 0)
end

function obj:right()
  positionWindow(hs.window.focusedWindow(), 1/2, 1, 1/2, 0)
end

function obj:top_left()
    positionWindow( hs.window.focusedWindow(), 1/2, 1/2, 0, 0 )
end

function obj:top_right()
    positionWindow( hs.window.focusedWindow(), 1/2, 1/2, 1/2, 0 )
end

function obj:bottom_left()
    positionWindow( hs.window.focusedWindow(), 1/2, 1/2, 0, 1/2 )
end

function obj:bottom_right()
    positionWindow( hs.window.focusedWindow(), 1/2, 1/2, 1/2, 1/2 )
end

function obj:move_left()
    moveWindow( hs.window.focusedWindow(), true, false, false, false, false )
end

function obj:move_right()
    moveWindow( hs.window.focusedWindow(), false, true, false, false, false )
end

function obj:move_top()
    moveWindow( hs.window.focusedWindow(), false, false, true, false, false )
end

function obj:move_bottom()
    moveWindow( hs.window.focusedWindow(), false, false, false, true, false )
end

function obj:nudge_left()
    moveWindow( hs.window.focusedWindow(), true, false, false, false, true )
end

function obj:nudge_right()
    moveWindow( hs.window.focusedWindow(), false, true, false, false, true )
end

function obj:nudge_up()
    moveWindow( hs.window.focusedWindow(), false, false, true, false, true )
end

function obj:nudge_down()
    moveWindow( hs.window.focusedWindow(), false, false, false, true, true )
end

function obj:max()
    hs.window.focusedWindow():maximize()
end

function obj:centre()
    hs.window.focusedWindow():centerOnScreen()
end

return obj
