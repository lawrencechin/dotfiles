--local switcher = require( 'switcher' )
local windows = require( 'window_management' )
local darkmode = require( 'darkmode' )

--hs.hotkey.bind({ "cmd" }, "`", switcher.cycleWindows )
--hs.hotkey.bind({ "ctrl", "alt" }, "left", windows.left )
--hs.hotkey.bind({ "ctrl", "alt" }, "right", windows.right )
--hs.hotkey.bind({ "ctrl", "alt" }, "u", windows.top_left )
--hs.hotkey.bind({ "ctrl", "alt" }, "i", windows.top_right )
--hs.hotkey.bind({ "ctrl", "alt" }, "j", windows.bottom_left )
--hs.hotkey.bind({ "ctrl", "alt" }, "k", windows.bottom_right )
--hs.hotkey.bind({ "ctrl", "alt" }, "m", windows.max )
--hs.hotkey.bind({ "ctrl", "alt" }, "c", windows.centre )
--hs.hotkey.bind({ "ctrl", "alt" }, "-", windows.shrink_horizontal )
--hs.hotkey.bind({ "ctrl", "alt" }, "=", windows.grow_horizontal )
--hs.hotkey.bind({ "ctrl", "alt", "shift" }, "left", windows.move_left )
--hs.hotkey.bind({ "ctrl", "alt", "shift" }, "right", windows.move_right )
--hs.hotkey.bind({ "ctrl", "alt", "shift" }, "up", windows.move_top )
--hs.hotkey.bind({ "ctrl", "alt", "shift" }, "down", windows.move_bottom )
--hs.hotkey.bind({ "ctrl", "alt", "shift" }, "-", windows.shrink_vertical )
--hs.hotkey.bind({ "ctrl", "alt", "shift" }, "=", windows.grow_vertical )
hs.hotkey.bind({ "ctrl", "alt", "shift" }, "s", windows.default_safari_size )
hs.hotkey.bind({ "ctrl", "alt", "shift" }, "f", windows.default_finder_size )
hs.hotkey.bind({ "ctrl", "alt", "shift" }, "h", windows.nudge_left )
hs.hotkey.bind({ "ctrl", "alt", "shift" }, "j", windows.nudge_down )
hs.hotkey.bind({ "ctrl", "alt", "shift" }, "k", windows.nudge_up )
hs.hotkey.bind({ "ctrl", "alt", "shift" }, "l", windows.nudge_right )

darkmode.addHandler( function() darkmode.updateDB( darkmode.getDarkMode()) end )

-- Utilities
-- disable window animations
hs.window.animationDuration = 0
