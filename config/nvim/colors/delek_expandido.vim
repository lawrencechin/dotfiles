" Vim color file
" Maintainer:   Lawrence Chin	
" Last Change:	2024 May 28
"
" Note - The main purpose of this theme is to work as a 
" pass through for the colours set in the terminal.
" This allows for an easy dark or light theme switch.
"
" Nb. White is equal to 15 (Bright White) and
" LightGray is equal to 7 (White)
" Grey is BrightBlack (8)
" These colour names are problematic.
" Switch ALL terminal profiles to reverse 7 -> 15?
"

highlight clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "delek_expandido"

" Normal should come first
hi Normal ctermfg=none

" Default Theme Groupings

hi Cursor ctermfg=Grey ctermbg=DarkCyan
hi CursorLineNr ctermbg=Grey
hi RedrawDebugNormal cterm=reverse
hi TabLineSel ctermfg=Blue cterm=bold,reverse
hi TermCursor cterm=reverse
hi Underlined ctermfg=DarkCyan cterm=underline
hi lcursor ctermfg=Grey ctermbg=DarkCyan

" Default Theme UI

hi! link CursorIM Cursor
hi! link CursorLineFold FoldColumn
hi! link CursorLineSign SignColumn
" hi EndOfBuffer ctermfg=Grey
hi FloatBorder ctermfg=Cyan ctermbg=none 
hi! link FloatFooter FloatTitle
hi! link FloatTitle Title
hi FoldColumn ctermfg=Cyan
hi IncSearch ctermfg=none ctermbg=none cterm=reverse 
hi! link LineNrAbove LineNr
hi! link LineNrBelow LineNr
hi MsgArea ctermfg=none ctermbg=none
hi! link MsgSeparator StatusLine
hi! link NormalNC NONE
hi! link PmenuExtra Pmenu
hi! link PmenuExtraSel PmenuSel
hi! link PmenuKind Pmenu
hi! link PmenuKindSel PmenuSel
hi PmenuSbar ctermbg=DarkGrey
hi! link Substitute IncSearch
hi TabLine ctermfg=Blue ctermbg=none cterm=underline
hi TabLineFill ctermfg=Blue ctermbg=none cterm=underline
hi TermCursorNC NONE
hi VertSplit ctermfg=Cyan ctermbg=none
hi VisualNOS ctermfg=none ctermbg=none cterm=reverse
hi Whitespace ctermfg=none
hi WildMenu ctermfg=none
hi! link WinSeparator Normal

" Default Theme Syntax

hi Boolean ctermfg=DarkYellow
hi Character ctermfg=DarkYellow
hi Conditional ctermfg=DarkYellow cterm=bold
hi Debug ctermfg=DarkRed ctermbg=none
hi Define ctermfg=DarkBlue
hi Exception ctermfg=DarkRed
hi Float ctermfg=DarkCyan
hi Ignore ctermfg=none
hi Include ctermfg=DarkBlue
hi! link Keyword Statement
hi Label ctermfg=Yellow
hi Macro ctermfg=DarkCyan
hi! link Number Constant
hi PreCondit ctermfg=Yellow
hi Repeat ctermfg=DarkBlue ctermbg=none
hi SpecialChar ctermfg=DarkCyan
hi SpecialComment ctermfg=Grey
hi StorageClass ctermfg=none
hi! link Structure Type
hi Tag ctermfg=DarkBlue
hi Typedef ctermfg=DarkCyan

" Default Theme LSP

hi! link LspCodeLens NonText
hi! link LspCodeLensSeparator LspCodeLens
hi! link LspInlayHint NonText
hi! link LspReferenceRead LspReferenceText
hi LspReferenceText ctermfg=none
hi! link LspReferenceWrite LspReferenceText
hi! link LspSignatureActiveParameter Visual
hi! link SnippetTabstop Visual

" Default Theme Diagnostic

hi DiagnosticFloatingError ctermfg=DarkRed
hi DiagnosticFloatingHint ctermfg=DarkYellow 
hi DiagnosticFloatingInfo ctermfg=Magenta
hi! link DiagnosticFloatingOk DiagnosticOk
hi DiagnosticFloatingWarn ctermfg=Yellow
hi DiagnosticSignError ctermfg=DarkMagenta
hi DiagnosticSignHint ctermfg=DarkYellow
hi DiagnosticSignInfo ctermfg=DarkBlue 
hi! link DiagnosticSignOk DiagnosticOk
hi DiagnosticSignWarn ctermfg=Yellow
hi! link DiagnosticUnnecessary Comment
hi DiagnosticVirtualTextError ctermfg=DarkRed
hi DiagnosticVirtualTextHint ctermfg=DarkYellow 
hi DiagnosticVirtualTextInfo ctermfg=Magenta
hi! link DiagnosticVirtualTextOk DiagnosticOk
hi DiagnosticVirtualTextWarn ctermfg=Yellow

" Default Theme Treesitter

hi @attribute ctermfg=Yellow
hi! link attribute.builtin Special
hi! link @boolean Boolean
hi! link @character Character
hi! link @character.special SpecialChar
hi! link @comment Comment
hi! link @comment.error DiagnosticError
hi! link @comment.note DiagnosticInfo
hi! link @comment.todo Todo
hi! link @comment.warning DiagnosticWarn
hi! link @constant Constant
hi! link @constant.builtin Constant
hi! link @constructor Special
hi! link @diff.delta Changed
hi! link @diff.minus Removed
hi! link @diff.plus Added
hi! link @float Float
hi @function ctermfg=DarkCyan
hi! link @function.builtin Function
hi! link @keyword Keyword
hi! link @label Label

hi! link @markup Special 

hi @markup.heading ctermfg=DarkBlue cterm=bold
hi @markup.heading.1 ctermfg=DarkBlue cterm=bold
hi @markup.heading.2 ctermfg=Blue cterm=bold
hi @markup.heading.3 ctermfg=Blue cterm=bold
hi @markup.heading.4 ctermfg=Blue cterm=bold
hi @markup.heading.5 ctermfg=Blue cterm=bold
hi @markup.heading.6 ctermfg=Blue cterm=bold
hi markdownHeadingDelimiter ctermfg=DarkMagenta
hi markdownHeadingRule ctermfg=DarkBlue cterm=bold

hi @markup.link ctermfg=none
hi @markup.link.label ctermfg=DarkMagenta
hi @markup.link.url ctermfg=Cyan
hi markdownLinkDelimiter ctermfg=Red
hi markdownLinkText ctermfg=none
hi markdownLinkTextDelimiter ctermfg=DarkMagenta
hi markdownUrl ctermfg=Cyan
hi markdownUrlDelimiter ctermfg=DarkMagenta
hi markdownUrlTitleDelimiter ctermfg=DarkGreen

hi @markup.strikethrough cterm=strikethrough
hi @markup.strong cterm=bold
hi @markup.underline cterm=underline
hi @markup.italic cterm=italic

hi @label.markdown ctermfg=DarkCyan
hi @punctuation.special.markdown ctermfg=DarkCyan
hi @markup.quote ctermfg=Red cterm=italic
hi @markup.raw ctermfg=Cyan
hi @markup.raw.block ctermfg=Cyan
hi markdownCodeDelimiter ctermfg=Grey

hi markdownId ctermfg=Blue
hi markdownIdDeclaration ctermfg=DarkGreen

hi @markup.list ctermfg=DarkRed
hi @markup.list.ordered ctermfg=DarkMagenta
hi markdownListMarker ctermfg=DarkRed
hi markdownOrderedListMarker ctermfg=DarkMagenta
hi markdownRule ctermfg=Grey

hi! link @module Structure
hi! link @module.builtin Special
hi! link @number Number
hi! link @number.float Float
hi! link @operator Operator
hi @property ctermfg=none
hi! link @punctuation Delimiter 
hi @punctuation.special ctermfg=Red
hi! link @string String
hi @string.escape ctermfg=Cyan
hi! link @string.regexp @string.special
hi! link @string.special SpecialChar
hi! link @string.special.url Underlined
hi! link @tag Tag
hi! link @tag.builtin Special
hi! link @type Type
hi @type.builtin ctermfg=Yellow
hi @variable ctermfg=none
hi @variable.builtin ctermfg=none
hi! link @variable.parameter.builtin Special

" Default Theme LSP

hi! link @lsp.type.class @type
hi! link @lsp.type.comment @comment
hi! link @lsp.type.decorator @attribute
hi! link @lsp.type.enum @type
hi! link @lsp.type.enumMember @constant
hi! link @lsp.type.event @type
hi! link @lsp.type.function @function
hi! link @lsp.type.interface Identifier
hi! link @lsp.type.keyword @keyword
hi! link @lsp.type.macro @constant.macro
hi! link @lsp.type.method @function.method
hi! link @lsp.type.modifier @type.qualifier
hi! link @lsp.type.namespace @namespace
hi! link @lsp.type.number @number
hi! link @lsp.type.operator @operator
hi! link @lsp.type.parameter @parameter
hi! link @lsp.type.property @property
hi! link @lsp.type.regexp @string.regexp
hi! link @lsp.type.string @string
hi! link @lsp.type.struct @type
hi! link @lsp.type.type @type
hi! link @lsp.type.typeParameter @type.definition
hi! link @lsp.type.variable @variable

" Cmp

hi! link CmpItemMenuDefault NormalFloat

" Telescope

hi TelescopeBorder ctermfg=Cyan
hi TelescopeMatching ctermfg=DarkMagenta
hi TelescopePreviewBorder ctermfg=Cyan
hi TelescopePromptBorder ctermfg=Cyan
hi TelescopePromptPrefix ctermfg=Blue
hi TelescopeResultsBorder ctermfg=Cyan
hi TelescopeSelection cterm=reverse
hi TelescopeSelectionCaret ctermfg=Blue
hi TelescopePrompt ctermfg=DarkMagenta

" Syntax

hi ColorColumn ctermfg=none ctermbg=Grey
hi CommandMode ctermfg=none
hi Comment ctermfg=DarkGrey cterm=italic
hi Conceal ctermfg=DarkCyan
hi Constant ctermfg=DarkCyan
hi CursorColumn ctermfg=none ctermbg=Grey
hi CursorLine ctermfg=none ctermbg=Grey
hi DashboardCenter ctermfg=none
hi DashboardFooter ctermfg=DarkGreen cterm=italic
hi DashboardHeader ctermfg=DarkYellow
hi DashboardShortCut ctermfg=DarkRed
hi Delimiter ctermfg=DarkGreen
hi DiffAdd ctermfg=DarkGreen ctermbg=none
hi DiffChange ctermfg=DarkYellow ctermbg=none
hi DiffDelete ctermfg=DarkRed ctermbg=none
hi DiffText ctermfg=none cterm=bold
hi Directory ctermfg=DarkBlue
hi Error ctermfg=DarkRed cterm=bold
hi ErrorMsg cterm=bold
hi FoldColumn ctermfg=Cyan
hi Folded ctermfg=Cyan
hi Function ctermfg=DarkMagenta
hi Identifier ctermfg=DarkCyan
hi IncSearch ctermfg=none ctermbg=none cterm=reverse
hi InsertMode ctermfg=DarkGreen
hi LineNr ctermfg=Grey
hi MatchParen ctermfg=Red cterm=underline
hi ModeMsg cterm=bold
hi MoreMsg ctermfg=Blue
hi NonText ctermfg=Red
hi NormalFloat ctermfg=none
hi NormalMode ctermfg=none
hi Operator ctermfg=Cyan
hi! link Pmenu NormalFloat
hi PmenuSel ctermfg=Yellow cterm=reverse,bold
hi PmenuThumb ctermbg=Grey
hi PreProc ctermfg=DarkMagenta
hi Question ctermfg=DarkGreen cterm=bold
hi Search ctermfg=DarkGreen cterm=bold,underline
hi SignColumn ctermfg=none ctermbg=none
hi Special ctermfg=DarkYellow
hi SpecialKey ctermfg=DarkBlue
hi SpellBad ctermfg=DarkRed cterm=undercurl
hi SpellCap ctermfg=DarkCyan cterm=undercurl
hi SpellLocal ctermfg=DarkBlue cterm=undercurl
hi SpellRare ctermfg=Yellow  cterm=undercurl
hi Statement ctermfg=blue cterm=bold
hi StatusLine ctermfg=DarkMagenta cterm=underline
hi StatusLineNC ctermfg=Grey cterm=underline
hi StatusLineTerm ctermfg=DarkBlue cterm=underline
hi! link StatusLineTermNC StatusLineNC
hi String ctermfg=DarkGreen ctermbg=none
hi Title ctermfg=DarkMagenta cterm=bold
hi Todo ctermfg=Yellow cterm=bold,italic
hi Type ctermfg=DarkBlue
hi Visual ctermfg=none ctermbg=none cterm=reverse
hi VisualMode ctermfg=none
hi WarningMsg ctermfg=DarkYellow
hi Warnings ctermfg=Yellow ctermbg=none cterm=reverse
hi healthError ctermfg=DarkRed
hi healthSuccess ctermfg=DarkGreen
hi healthWarning ctermfg=DarkYellow
hi qfLineNr ctermfg=Yellow ctermbg=none

" Treesitter

hi! link @annotation Comment
hi! link @conditional Conditional
hi! link @constant.macro Constant
hi! link @constructor PreProc
hi! link @error Error
hi! link @exception Exception
hi @field ctermfg=none
hi @function.macro ctermfg=none
hi! link @include Include
hi! link @keyword.function Function
hi @literal ctermfg=none
hi! link @lsp.typemod.function.defaultLibrary @function.builtin
hi! link @lsp.typemod.method.defaultLibrary @function.builtin
hi! link @lsp.typemod.operator.injected @operator
hi! link @lsp.typemod.string.injected @string
hi! link @lsp.typemod.variable.defaultLibrary @variable.builtin
hi! link @lsp.typemod.variable.injected @variable
hi @method ctermfg=DarkCyan
hi @namespace ctermfg=Yellow
hi! link @operator Operator
hi @parameter ctermfg=Cyan
hi @parameter.reference ctermfg=Cyan
hi! link @punctuation.bracket PreProc
hi @punctuation.delimiter ctermfg=Cyan
hi! link @repeat Repeat
hi! link @structure Structure
hi @symbol ctermfg=Yellow
hi @tag.delimiter ctermfg=Cyan
hi @text ctermfg=none
hi @text.emphasis ctermfg=none cterm=italic
hi @text.reference ctermfg=none
hi @text.strike ctermfg=DarkRed cterm=strikethrough
hi @text.strong ctermfg=none cterm=bold
hi @text.underline ctermfg=none cterm=underline
hi @title ctermfg=DarkMagenta cterm=bold
hi @uRI ctermfg=DarkCyan

" LSP

hi DiagnosticError ctermfg=DarkRed
hi DiagnosticHint ctermfg=DarkYellow 
hi DiagnosticInfo ctermfg=Magenta
hi DiagnosticWarn ctermfg=Yellow
hi DiagnosticUnderlineError cterm=undercurl ctermfg=DarkRed
hi DiagnosticUnderlineHint cterm=undercurl ctermfg=DarkYellow
hi DiagnosticUnderlineInfo cterm=undercurl ctermfg=Magenta
hi DiagnosticUnderlineWarn cterm=undercurl ctermfg=Yellow

" Lazy

hi LazyButton ctermfg=DarkBlue ctermbg=none
hi LazyButtonActive ctermbg=DarkBlue ctermfg=7 cterm=bold
hi LazyH1 cterm=bold ctermfg=7 ctermbg=DarkBlue
hi LazyH2 cterm=bold
hi LazySpecial ctermfg=DarkMagenta

" vim: sw=2
