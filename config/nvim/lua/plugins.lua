-- Install Lazy if not already 

local fn = vim.fn
local lazypath = fn.stdpath( "data" ) .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat( lazypath ) then
    fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",
        lazypath,
    })
end

vim.opt.rtp:prepend( lazypath )

require( "lazy" ).setup({
    { "folke/which-key.nvim",
        config = function()
            require( "which-key" ).setup({
                opts = {},
                icons = { 
                    rules = false,
                    keys = {
                        Up = "􀄨 ",
                        Down = "􀄩 ",
                        Left = "􁉄  ",
                        Right = "􁉂  ",
                        C = "􀆍 ",
                        M = "􀆔  ",
                        S = "􀆕  ",
                        CR = "􀋾  ",
                        Esc = "􀆧  ",
                        ScrollWheelDown = "􀻘  ",
                        ScrollWheelUp = "􀻖  ",
                        NL = "􁙀  ",
                        BS = "􀆛  ",
                        Space = "􁁺  ",
                        Tab = "􀆎  "
                    }
                },
                win = { border = "rounded" }
            })
        end
    },
    { "neovim/nvim-lspconfig" },
    { "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-nvim-lua",
            "hrsh7th/cmp-path"
        },
        config = function()
            local cmp = require "cmp"

            cmp.setup({
                enabled = function()
                    return vim.g.cmp_enable_toggle
                end,
                window = {
                    documentation = cmp.config.window.bordered(),
                    completion = cmp.config.window.bordered()
                },
                formatting = {
                    format = function( entry, vim_item )
                        -- Icons
                        vim_item.kind = string.format( '%s %s', _G.utils.icons[vim_item.kind], vim_item.kind )
                        vim_item.menu = ({
                            buffer = "[Buffer]",
                            nvim_lsp = "[LSP]",
                            nvim_lua = "[Lua]",
                        })[entry.source.name]
                        return vim_item
                    end
                },
                snippet = {
                    expand = function( args )
                        vim.snippet.expand( args.body )
                    end,
                },
                mapping =  cmp.mapping.preset.insert ({
                    [ '<C-b>' ] = cmp.mapping.scroll_docs( -4 ),
                    [ '<C-f>' ] = cmp.mapping.scroll_docs( 4 ),
                    [ '<C-Space>' ] = cmp.mapping.complete(),
                    [ '<C-e>' ] = cmp.mapping.abort(),
                    [ '<C-n>' ] = cmp.mapping( cmp.mapping.select_next_item(), { "i", "c" }),
                    [ '<C-p>' ] = cmp.mapping( cmp.mapping.select_prev_item(), { "i", "c" }),
                    [ '<C-x><C-o>' ] = cmp.mapping.complete(),
                    [ '<CR>' ] = cmp.mapping.confirm({ 
                        select = false,
                        behavior = cmp.ConfirmBehavior.Replace,
                    }),
                    [ '<Tab>' ] = cmp.mapping( function( fallback )
                        if cmp.visible() then
                            cmp.select_next_item()
                        else
                            fallback()
                        end
                    end, { "i", "s" }),
                    [ '<S-Tab>' ] = cmp.mapping( function( fallback )
                        if cmp.visible() then
                            cmp.select_prev_item()
                        else
                            fallback()
                        end
                    end, { "i", "s" }),
                }),
                sources = cmp.config.sources({ 
                    { name = "buffer", priority = 20 },
                    { name = "calc" },
                    { name = "nvim_lsp", priority = 80 },
                    { name = "nvim_lua", priority = 80 },
                    { name = "path", priority = 60 },
                }),
            });
        end
    },
    { "nvim-treesitter/nvim-treesitter",
        config = function()
            require( "nvim-treesitter.configs" ).setup({
                ensure_installed = { "bash", "css", "fish", "html", "javascript", "json", "lua", "python" },
                highlight = {
                    enable = true,
                }
            })
        end
    },
    { "nvim-telescope/telescope.nvim",
        branch = "0.1.x",
        dependencies = { 'nvim-lua/plenary.nvim' },
        keys = {
            { "<leader>ff", ":Telescope find_files theme=dropdown<cr>", desc = "Find Files" },
            { "<leader>fg", ":Telescope live_grep theme=dropdown<cr>", desc = "Live Grep" },
            { "<leader>fb", ":Telescope buffers theme=ivy<cr>", desc = "Buffers" },
            { "<leader>fh", ":Telescope help_tags<cr>", desc = "Help Tags" },
            { "<leader>ft", ":Telescope treesitter theme=ivy<cr>", desc = "Treesitter" },
            { "<leader>fd", ":Telescope diagnostics theme=ivy<cr>", desc = "Diagnostics" },
            { "<leader>fs", ":Telescope lsp_document_symbols theme=ivy<cr>", desc = "Document Symbols" }
        },
        config = function()
            require( "telescope" ).setup()
        end
    }
    }, 
    {
        lockfile = fn.stdpath( "data" ) .. "/lazy/lazy-lock.json",
        ui = { 
            border = "rounded",
            pills = true,
            icons = {
                cmd = "􀩼  ",
                config = "􀣋  ",
                event = "􁓵   ",
                ft = "􀈫  ",
                init = "􀇲  ",
                import = "􀉢  ",
                keys = "􀟕  ",
                lazy = "􀖃  ",
                loaded = "•",
                not_loaded = "°",
                plugin = "􁌥  ",
                runtime = "􀐳  ",
                require = "􀸘  ",
                source = "􁈹  ",
                start = "􁓂  ",
                task = "􁂾  ",
                list = {
                    "●",
                    "➜",
                    "★",
                    "‒",
                },
            }
        }
    }
)

vim.opt.rtp:prepend( fn.stdpath( "data" ) .. "/site" )
