--
-- Autocmd
--

vim.api.nvim_create_autocmd( "TermOpen", {
    pattern = "*",
    callback = function()
        vim.wo.wrap = "wrap"
    end,
})

--
-- Commands
--

-- Shortcut for changing pwd for current open file
vim.api.nvim_create_user_command( "CDC", "cd %:p:h", { nargs = 0 })

