--
-- Set Options
--

local cmd = vim.cmd -- to execute Vim commands
local opt = vim.opt -- to set options
local g = vim.g -- a table to access global variables

-- Improve performance
opt.cursorline = false -- don't paint cursor line
opt.cursorcolumn = false -- don't paint cursor column
-- opt.lazyredraw = false -- don't wait to redraw
opt.showcmd = false

opt.completeopt = "menuone,noselect" -- necessary for nvim-cmp
opt.number = false
opt.relativenumber = false
opt.encoding = "utf-8"
opt.spell = false -- off by default, toggles with leader-sp
opt.spelllang = { "en", "es" }
opt.autoread = true -- auto read file if changed outside of nvim
opt.joinspaces = true --insert spaces after '.?!' when joining lines
opt.clipboard = "unnamed,unnamedplus"
--opt.laststatus = 3 -- set global status bar
opt.shortmess:append {c = true}
opt.signcolumn = "yes:1"

-- Searching
opt.hlsearch = false -- Search as characters are entered
opt.incsearch = true -- Highlight matches
opt.ignorecase = true
opt.smartcase = true -- case sensitive search when including capitals

-- Backups - all sent to /tmp
local bck_dir = os.getenv( "TMPDIR" )
opt.backup = true
opt.undofile = true
opt.backupdir = bck_dir .. "/"
opt.undodir = bck_dir .. "/"
opt.directory = bck_dir .. "/"

-- Set Python3 path
g.python3_host_prog = "/Users/$USER/.penv/venv/bin/python3"
g.python3_host_skip_check = 1

-- Disable providers we do not use
g.loaded_python_provider = 0
g.loaded_ruby_provider = 0
g.loaded_perl_provider = 0
g.loaded_node_provider = 0

opt.tabstop = 4
opt.shiftwidth = 4
opt.expandtab = true

-- Colours
g.markdown_fenced_languages = {
    'bash=sh',
    'css',
    'fish=sh',
    'html',
    'javascript',
    'json',
    'lua',
    'python',
    'ruby',
    'sh',
    'sql',
    'vim',
    'zsh=sh',
}

-- opt.termguicolors = true
opt.termguicolors = false -- using cterm colours only
-- g.fresh_air_transparent_background = true
-- g.arsenixc_transparent_background = true
g.cmp_enable_toggle = false
vim.cmd "colorscheme delek_expandido"
