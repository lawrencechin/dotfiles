-- Archive for Nerd Font icons
-- Statusline
function get_mode_group(m)
    -- to get something like  , do ctrl-v then press key
    local mode_groups = {
        n = " Normal",
        no = " Nop",
        nov = " Nop",
        noV = " Nop",
        ["noCTRL-V"] = " Nop",
        niI = " Normal",
        niR = " Normal",
        niV = " Normal",
        v = "󰸌 Visual-Char",
        V = "󰸌 Visual-Line",
        [""] = "󰸌 Visual-Block",
        s = " Select-Char",
        S = " Select-Line",
        [""] = " Select-Block",
        i = " Insert",
        ic =" Insert",
        ix = " Insert",
        R = "󰴂 Replace",
        Rc = "󰴂 Replace",
        Rv = "󰴂 Replace",
        Rx = " Replace",
        c = " Command",
        cv = " Command",
        ce = " Command",
        r = " Prompt",
        rm = " Prompt",
        ["r?"] = " Prompt",
        ["!"] = " Shell",
        t = " Term",
        ["null"] = "󰯈 None"
    }
    return mode_groups[m]
end

-- LSP
local signs = { Error = "", Warn = "", Hint = "", Info = "" }

-- Utils
M.icons = {
    Class = "󰠲",
    Color = "󰸌",
    Constant = "󰏿",
    Constructor = "",
    Enum = "",
    EnumMember = "",
    Event = "",
    Field = "󰜢",
    File = "󰈙",
    Folder = "",
    Function = "󰡱",
    Interface = "",
    Keyword = "",
    Method = "󰆧",
    Module = "",
    Operator = "󰆕",
    Property = "󰜢",
    Reference = "󰈇",
    Snippet = "",
    Struct = "󱏒",
    Text = "󰛘",
    TypeParameter = "",
    Unit = "󰑭",
    Value = "󰎠",
    Variable = "",
}
