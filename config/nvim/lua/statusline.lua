--
-- Statusline
--

local o = vim.o -- a table to access global variables
local fn = vim.fn
local api = vim.api
--local icons = require( "nvim-web-devicons" )

-- sensibly group the vim modes
function get_mode_group(m)
    -- to get something like  , do ctrl-v then press key
    local mode_groups = {
        n = "􀯏  Normal",
        no = "􀯏  Nop",
        nov = "􀯏  Nop",
        noV = "􀯏  Nop",
        ["noCTRL-V"] = "􀯏  Nop",
        niI = "􀯏  Normal",
        niR = "􀯏  Normal",
        niV = "􀎸  Normal",
        v = "􀝥  Visual-Char",
        V = "􀝥  Visual-Line",
        [""] = "􀝥  Visual-Block",
        s = "􀖆  Select-Char",
        S = "􀠍  Select-Line",
        [""] = "􀠍  Select-Block",
        i = "􀞺  Insert",
        ic ="􀞺  Insert",
        ix = "􀞺  Insert",
        R = "􀺧  Replace",
        Rc = "􀺧  Replace",
        Rv = "􀺧  Replace",
        Rx = "􀺧  Replace",
        c = "􀩼  Command",
        cv = "􀩼  Command",
        ce = "􀩼  Command",
        r = "􀪏  Prompt",
        rm = "􀪏  Prompt",
        ["r?"] = "􀪏  Prompt",
        ["!"] = "􀪏  Shell",
        t = "􀪏  Term",
        ["null"] = "􁐾  None"
    }
    return mode_groups[m]
end

-- get the display name for the group
function get_mode_group_display_name(mg)
    return mg:upper()
end

local function get_icon_by_filetype()
    local file_name, file_ext = fn.expand("%:t"), fn.expand("%:e")
    local icon = icons.get_icon(file_name, file_ext, { default = true })
    local filetype = vim.bo.filetype

    if filetype == '' then return '' end
    return string.format(' %s %s ', icon, filetype):lower()
end

function status_line()
    -- local mode = fn.mode()
    local mode = api.nvim_get_mode().mode
    local mg = get_mode_group(mode)
    local focus = vim.g.statusline_winid == fn.win_getid()
    mode_text = get_mode_group_display_name(mg)

    if focus then -- active
        return table.concat {
            "%#StatusLeft#",
            "% " ..mode_text .. " > ",
            "%#StatusMid#",
            "🙊 > %t%m%<",
            "%=",
            "%#StatusRight#",
            "| %p%% | %l:%c%"
        }
    else
        return table.concat {
            "🙈 ",
            "%=",
            "| %p%% | %l:%c%"
        }
    end

end

vim.o.statusline = "%!luaeval('status_line()')"
