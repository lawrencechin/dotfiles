local M = {} -- module object to export

function M.is_darwin()
    local os_name = vim.loop.os_uname().sysname
    return os_name == "Darwin"
end

function M.current_kitty_theme()
    --local kitty_theme_name = vim.fn.system( "sqlite3 ~/.local/share/wallpapers.db 'SELECT Themes.nvim_alt_name FROM Themes, Selected WHERE Themes.id = Selected.theme_id;'" )
    local kitty_theme_name = vim.fn.system({ 
        'sqlite3',
        os.getenv( "HOME" ) .. '/.local/share/wallpapers.db', 
        'SELECT Themes.nvim_alt_name FROM Themes, Selected WHERE Themes.id = Selected.theme_id;' 
    })
    kitty_theme_name = kitty_theme_name:gsub( "%s+", "" )
    local fresh_air = "fresh_air.functions"
    local arsenixc = "arsenixc.functions"
    local fresh_air_themes = require ( fresh_air ).list_themes()
    local arsenixc_themes = require ( arsenixc ).list_themes()

    if vim.api.nvim_get_vvar( "shell_error" ) ~= 0 then
        -- couldn't get kitty theme name
        return { colour = fresh_air, light = "fresh_air", dark = "crisp_night" }
    else
        for _, theme in pairs( fresh_air_themes ) do
            if theme == kitty_theme_name then
                return { colour = fresh_air, light = kitty_theme_name, dark = kitty_theme_name }
            end
        end

        for  _,theme in pairs( arsenixc_themes ) do
            if theme == kitty_theme_name then
                return { colour = arsenixc, light = kitty_theme_name, dark = kitty_theme_name }
            end
        end
        -- theme not found, return default light and dark theme
        return { colour = fresh_air, light = "fresh_air", dark = "crisp_night" }
    end
end

M.icons = {
    Class = "􀫓 ",
    Color = "􀝥 ",
    Constant = "􀎠 ",
    Constructor = "􀣋 ",
    Enum = "􀼏 ",
    EnumMember = "􁕜 ",
    Event = "􁓵 ",
    Field = "􀋡 ",
    File = "􀈿 ",
    Folder = "􀈕 ",
    Function = "􀅮 ",
    Interface = "􀫥 ",
    Keyword = "􀫖 ",
    Method = "􀎕 ",
    Module = "􁕠 ",
    Operator = "􀓪 ",
    Property = "􀋡 ",
    Reference = "􀣪 ",
    Snippet = "􀤏 ",
    Struct = "􀫐 ",
    Text = "􀌮 ",
    TypeParameter = "􀙤 ",
    Unit = "􁖆 ",
    Value = "􁂷 ",
    Variable = "􀎤 ",
}

-- Not sure what this was used for
local has_words_before = function()
    local line, col = unpack( vim.api.nvim_win_get_cursor( 0 ))
    return col ~= 0 and vim.api.nvim_buf_get_lines( 0, line - 1, line, true )[ 1 ]:sub( col, col ):match( "%s" ) == nil
end

_G.utils = M --can be accessed in all configuration files using global variable
return M
