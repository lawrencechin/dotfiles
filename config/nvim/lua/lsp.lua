--
-- LSP Settings
--

local nvim_lsp = require( "lspconfig" )

local capabilities = require( "cmp_nvim_lsp" ).default_capabilities( vim.lsp.protocol.make_client_capabilities())

-- Mappings
function opts( description )
    return { noremap = true, silent = true, desc = description }
end

vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts( "Open Diagnostic Window" ))
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts( "Set LocList Diagnostics" ))

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    function bufopts( description )
        return { noremap = true, silent = true, buffer=bufnr, desc = description }
    end

    vim.keymap.set( 'n', 'gD', vim.lsp.buf.declaration, bufopts( "LSP Declaration" ))
    vim.keymap.set( 'n', 'gd', vim.lsp.buf.definition, bufopts( "LSP Definition" ))
    vim.keymap.set( 'n', 'gi', vim.lsp.buf.implementation, bufopts( "Implementations" ))
    vim.keymap.set( 'n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts( "Add Workspace Folder" ))
    vim.keymap.set( 'n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts( "Remove Workspace Folder" ))
    vim.keymap.set( 'n', '<space>wl', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, bufopts( "List Workspace Folders" ))
    vim.keymap.set( 'n', '<space>D', vim.lsp.buf.type_definition, bufopts( "Type Definition" ))
    vim.keymap.set( 'n', '<space>br', vim.lsp.buf.rename, bufopts( "Buffer Rename" ))
    vim.keymap.set( 'n', '<space>ba', vim.lsp.buf.code_action, bufopts( "Code Action" ))
    vim.keymap.set( 'n', 'gr', vim.lsp.buf.references, bufopts( "Buffer References" ))
    vim.keymap.set( "n", "<space>bf", vim.lsp.buf.format, bufopts( "Buffer Format" ))

end

local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
    opts = opts or {}
    opts.border = "rounded"
    return orig_util_open_floating_preview(contents, syntax, opts, ...)
end

local servers = { "jsonls", "cssls", "html", "bashls", "pyright" }

for _, lsp in ipairs( servers ) do
    nvim_lsp[ lsp ].setup {
        on_attach = on_attach,
        capabilities = capabilities
    }
end

nvim_lsp.ts_ls.setup {
    on_attach = on_attach,
    root_dir = function() return vim.loop.cwd() end,
    capabilities = capabilities
}

local signs = { Error = "􀅎", Warn = "􀋦", Hint = "􁒰", Info = "􀁜" }

for type, icon in pairs( signs ) do
    local hl = "DiagnosticSign" .. type
    vim.fn.sign_define( hl, { text = icon, texthl = hl, numhl = "" })
end

vim.diagnostic.config({
    virtual_text = false,
    signs = true,
})
