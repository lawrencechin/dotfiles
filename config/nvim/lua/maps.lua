--
-- Mappings
--

local g = vim.g -- a table to access global variables
local map = vim.api.nvim_set_keymap -- shortcut for mapping keys
local wrap = require ( "utils" ).wrap

-- Leader to <space>
g.mapleader = " "
g.maplocalleader = " "

-- Map Callbacks
local toggleCmp = function()
    g.cmp_enable_toggle = not( g.cmp_enable_toggle )
end

local toggleSpell = function()
    vim.opt.spell = not( vim.opt.spell:get() )
end

map( "n", "/", [[/\v]], { noremap = true, desc = "Use very magic for searching" })
map( "v", "/", [[/\v]], { noremap = true, desc ="Use very magic for search" })

map( "v", ".", ":norm.<CR>", { noremap = true, desc = "Repeat action over selected lines in Visual"})

-- Split navigation
map( "n", "<C-J>", "<C-W><C-J>", { noremap = true, desc = "Select next split below" })
map( "n", "<C-K>", "<C-W><C-K>", { noremap = true, desc = "Select previous split above" })
map( "n", "<C-L>", "<C-W><C-L>", { noremap = true, desc = "Select next split" })
map( "n", "<C-H>", "<C-W><C-H>", { noremap = true, desc = "Select previous split" })

map( "n", "<leader>bd", ":bp<cr>:bd #<cr>", { noremap = true, desc = "Remove buffer without changing split arrangement" })

map( "n", "<Leader>z", "viw<ESC>a<C-x>s", { noremap = true, desc = "Spelling suggestions in floating window" })
map( "n", "<Leader>sp", "", { 
    noremap = true,
    callback = toggleSpell,
    desc = "Toggle spell check"
})

map( "t", "<C-Space>", [[<c-\><c-n>]], { noremap = true, desc = "Exit terminal insert mode" })

-- Toggle cmp
map( "n", "<Leader>tc", "", { 
    noremap = true,
    callback = toggleCmp,
    desc = "Toggle auto completion"
})
