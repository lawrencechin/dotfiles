function misc --description "grab bag of functions"
    set -l safari_caches "$HOME/Library/Containers/com.apple.Safari/Data/Library/WebKit/WebsiteDataStore"
    set -l web_apps_caches "$HOME/Library/Containers/com.apple.Safari.WebApp/Data/Library/Containers"

    options_list
    set -l prompt "􀌮  > "
    
    while true
        read -f input -P "$prompt"
        switch "$input"
            case 1
                echo -e "􀌮  Safari and Web Apps and using this much cache\n"
                echo -e "􀵱  Safari (all profiles): \n"
                safari_cache_sizes "$safari_caches"
                echo -e "\n􀵵  Web Apps: \n"
                safari_cache_sizes "$web_apps_caches"
            case 2
                echo "􀌮  Clearing Safari caches…"
                safari_cache_clean "$safari_caches"
                echo "􀌮  Clearing Web App caches…"
                safari_cache_clean "$web_apps_caches"
                echo "􀌮  Empty recycle bin to finish"
            case 3
                dupe_guru_vacuum
            case 4
                set -l prom_pt "Search Term > "
                read -l st  -P "$prom_pt"
                app_leftovers "$st"
            case "q"
                break
            case '*'
                options_list
        end
    end
end

function options_list
    set_color -o; printf "\nSelect (numeric) options\n"; set_color normal
    set_color magenta -i; printf "1. "; set_color normal; printf "Get Safari Cache Sizes\n" 
    set_color blue -i; printf "2. "; set_color normal; printf "Delete Safari Caches\n" 
    set_color green -i; printf "3. "; set_color normal; printf "Vacuum Dupe Guru database\n" 
    set_color yellow -i; printf "4. "; set_color normal; printf "Search for Application leftovers in Library\n" 
    set_color red -i; printf "q. "; set_color normal; printf "Quit\n" 
end

function safari_cache_clean --description "Clean caches from Safari or Safari web apps"
    if test -d "$argv[1]"
        fd -t d "NetworkCache" "$argv[1]" -x trash {}
        #fd -t d "NetworkCache" "$argv[1]" -x rm -rf {}
    end
end

function safari_cache_sizes --description "Get size of caches"
    if test -d "$argv[1]"
        fd -t d "NetworkCache" "$argv[1]" -x  du -h -d0 {} | sed -E 's/([0-9]+\.?([0-9]+)?[A-Z]).+/\1/'
    else
        echo "Wha"
    end
end

function dupe_guru_vacuum --description "Vacuum dupe gurus sqlite db"
    set -l cached_picture_path "$HOME/Library/Application Support/Hardcoded Software/dupeGuru/cached_pictures.db"
    set -l hash_cache_path "$HOME/Library/Application Support/Hardcoded Software/dupeGuru/hash_cache.db"

    set_color -o; printf "Current sizes\n\n"; set_color normal
    du -h "$cached_picture_path"
    du -h "$hash_cache_path"
    set_color -i; printf "\nVacuuming…\n"; set_color normal
    sqlite3 "$cached_picture_path" 'VACUUM;'
    sqlite3 "$hash_picture_path" 'VACUUM;'
    set_color -o; printf "\nNew sizes\n"; set_color normal
    du -h "$cached_picture_path"
    du -h "$hash_cache_path"
end

function app_leftovers --description "Search library for left overs from application"
    fd "$argv[1]" ~/Library
end
