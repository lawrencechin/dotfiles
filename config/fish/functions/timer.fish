function timer --description 'Set a timer'
    if test -f ~/.penv/venv/bin/termdown
	    $UTILS/timer.sh $argv
    else
        echo "Termdown not installed. Just use the ⏰ app!"
    end
end
