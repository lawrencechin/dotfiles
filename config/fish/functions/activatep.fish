function activatep --description "Source Virtual Python Environment"

    if test -d $HOME/.penv/venv
        source $HOME/.penv/venv/bin/activate.fish
    end
end
        
