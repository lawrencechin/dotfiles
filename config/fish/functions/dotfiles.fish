function dotfiles --description 'Symlink dotfiles to HOME'
    echo "Symlinking dotfiles into HOME 🏠"
    read res -f -n 1 -P "Proceed ? (y | n) ~> "
    if test "$res" = "y"
        echo "Sym🔗ing…"
	    $DOTFILES/dotfiles_link.sh 
    end
end
