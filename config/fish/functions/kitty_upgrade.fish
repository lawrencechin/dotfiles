function kitty_upgrade --description "Official Kitty install/upgrade Script"
    if test -d ~/Applications/Kitty.app
        curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
    else
        echo "😿 -> Kitty not installed"
    end
end
