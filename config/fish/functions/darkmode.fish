function darkmode --description "Set terminal theme and print current theme"
    argparse s h -- $argv
    if set -q _flag_s
        set -l db ~/.local/share/wallpapers.db
        set -l term_theme_name ( sqlite3 "$db" 'SELECT Themes.name FROM Themes, Selected WHERE Themes.id = Selected.theme_id;' )
        set -l type_name ( sqlite3 "$db" 'Select Type.font_name FROM Type, Selected WHERE Type.id = Selected.type_id;' )
        set -l type_size ( sqlite3 "$db" 'Select Type.font_size FROM Type, Selected WHERE Type.id = Selected.type_id;' )

        if test -d ~/Applications/Kitty.app
            # Old method
            # kitty +kitten themes --reload-in=all "$kitty_theme_name"
            cat ~/.config/kitty/themes/"$term_theme_name".conf > ~/.config/kitty/current-theme.conf
            kitty +runpy "from kitty.utils import *; reload_conf_in_all_kitties()"
        end

        osascript -e "if application \"Terminal\" is running then" -e "tell application \"Terminal\"" -e "repeat with w in windows" -e "repeat with t in tabs of w" -e "set current settings of t to settings set \"$term_theme_name\"" -e "set font name of current settings of t to \"$type_name\"" -e "set font size of current settings of t to \"$type_size\"" -e "end repeat" -e "end repeat" -e "end tell" -e "end if"

    else if set -q _flag_h
        echo "Options:"
        echo "-h : Display list of arguments"
        echo "-s : Set Terminal Theme"
        echo "Without arguments : Display current theme"
    else
        set -l theme_name ( sqlite3 ~/.local/share/wallpapers.db 'SELECT Themes.name from Themes, Selected WHERE Themes.id = Selected.theme_id;')
        set -l mode ( sqlite3 ~/.local/share/wallpapers.db 'SELECT type FROM Mode WHERE id = ( SELECT Themes.mode_id FROM Themes, Selected WHERE Themes.id = Selected.theme_id )' )
        set -l font ( sqlite3 ~/.local/share/wallpapers.db 'SELECT font_name from Type WHERE id = ( SELECT type_id from Selected )' )
        set -l font_size ( sqlite3 ~/.local/share/wallpapers.db 'SELECT font_size FROM Type WHERE id = ( SELECT type_id from Selected )' )

        echo (set_color blue)$mode(set_color normal): (set_color --bold)$theme_name(set_color normal) 􀝥  (set_color black) • (set_color brblack)• (set_color red)• (set_color brred)• (set_color green)• (set_color brgreen)• (set_color yellow)• (set_color bryellow)•
        if test "$mode" = "light"
            set -f spaced_type "font "
        else
            set -f spaced_type "font"
        end

        echo (set_color blue)$spaced_type(set_color normal): "$font("(set_color -i)$font_size(set_color normal)")" (set_color blue)• (set_color brblue)• (set_color magenta)• (set_color brmagenta)• (set_color cyan)• (set_color brcyan)• (set_color white)• (set_color brwhite)•(set_color normal)

   end
end
