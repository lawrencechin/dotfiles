function gatekeeper --description 'Display menu to control GateKeeper in macOS'
	$UTILS/gatekeeper.sh
end
