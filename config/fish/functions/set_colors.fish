function set_fish_theme --description "Set fish colours"
    set -U fish_color_autosuggestion brblack
    set -U fish_color_cancel normal
    set -U fish_color_command blue
    set -U fish_color_comment green --italics
    set -U fish_color_cwd magenta
    set -U fish_color_cwd_root red
    set -U fish_color_end brcyan
    set -U fish_color_error brred
    set -U fish_color_escape cyan
    set -U fish_color_history_current --bold
    set -U fish_color_host normal
    set -U fish_color_host_remote yellow
    set -U fish_color_match normal
    set -U fish_color_normal normal
    set -U fish_color_operator cyan
    set -U fish_color_param brblue
    set -U fish_color_quote yellow --italics
    set -U fish_color_redirection cyan --bold
    set -U fish_color_search_match brblack
    set -U fish_color_selection brwhite
    set -U fish_color_user green
    set -U fish_color_valid_path normal
end
