function dadjoke --description "Fetch a new Dad Joke, yay"
    set -l joke_response ( curl -s --connect-timeout 0.5 --no-progress-meter -H "Accept: text/plain" https://icanhazdadjoke.com/ )
    if test -n "$joke_response"
        echo (set_color magenta)􀎸 􀌮  (set_color --italics brblack) $joke_response 
        set_color normal
    else
        echo (set_color green)􀯏 "> "(set_color --italics brblack)(fortune)
        set_color normal
    end
end
