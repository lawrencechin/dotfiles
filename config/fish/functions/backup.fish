function backup --description 'Backup apps and user folders for Mandy'

    echo "Backing up Applications and User folder to macOS_bck"
    read y -l -n 1 -P "Proceed (y / n)? ~> "
    if test "$y" = "y"
        if [ -d /Volumes/macOS_bck/ ]
            if [ -d /Volumes/Krangy/ ]
                # Krangy Users (includes user Applications)
                sudo time rsync -A -v -X -H -p --fileflags --force-change -l -N -rtx --numeric-ids -go --delete-during --protect-args --exclude-from "/Volumes/macOS_bck/exclude_list.txt" /Volumes/Krangy/Users /Volumes/macOS_bck/Krang
            end
        else
            echo "Backup drive not mounted 🦉"
        end
    end
end
