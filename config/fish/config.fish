# Variables

set -gx EDITOR nvim
set -gx PAGER less
set -gx VISUAL nvim
set -gx HOMEBREW_NO_AUTO_UPDATE 1
set -gx HOMEBREW_DISPLAY_INSTALL_TIMES 1
set -gx HOMEBREW_NO_GOOGLE_ANALYTIC 1
set -gx HOMEBREW_NO_INSTALL_CLEANUP 1

# Less Colors

set -x LESS_TERMCAP_mb (set_color brred)
set -x LESS_TERMCAP_md (set_color brred)
set -x LESS_TERMCAP_me (set_color normal)
set -x LESS_TERMCAP_se (set_color normal)
set -x LESS_TERMCAP_so (set_color -b blue bryellow)
set -x LESS_TERMCAP_ue (set_color normal)
set -x LESS_TERMCAP_us (set_color brgreen)
set -x LS_COLORS ''

# Paths

set -gx DEV ~/.dev
set -gx UTILS $DEV/@backup/scripts/@utils
set -gx DOTFILES $DEV/@dotfiles/
set -gx NOTES $DEV/Notes/Notes

if test (uname) = "Darwin"
    set -gx ICDOCS "$HOME/Library/Mobile Documents/com~apple~CloudDocs/"
    set -gx ICDOCSESC "$HOME/Library/Mobile\ Documents/com~apple~CloudDocs/"
    set -gx ICSCRIPTS "$HOME/Library/Mobile Documents/com~apple~ScriptEditor2/Documents/"
end # Add variables for other systems as else if

# Settings

# Fix Ctrl-F autosuggestions when using vi-mode
# Doesn't seem to work when set as a function in the autoload folder
function fish_user_key_bindings
    bind -M insert \cf accept-autosuggestion
    bind \cf accept-autosuggestion
end
# Function before bindings call is important!
fish_vi_key_bindings
