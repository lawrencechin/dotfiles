#LS and OS-based
case "$(uname)" in
    Linux)
        alias ls='ls --color=auto' ;;
    FreeBSD)
        alias ls='ls -G' ;;
    Darwin)
        alias ls="ls -G" ;
        alias battery="$UTILS/battery_status.sh" ;
        alias gatekeeper="$UTILS/gatekeeper.sh" ;;
esac

# Scripts
alias sys_refresh="$DEV/@backup/scripts/Update_and_install_files.sh"
alias radio="$DEV/Radio_Stations/radio.sh"
alias timer="$UTILS/timer.sh"
alias updateLocal="$UTILS/update_local.sh"
alias dotfiles="$DOTFILES/dotfiles_link.sh"


alias l='ls -lFh'     #size,show type,human readable
alias la='ls -lAFh'   #long list,show almost all,show type,human readable

#EDITORS
alias e='nvim'
alias vim='nvim'
alias vi='nvim'

#COLORFULNESS
alias grep="grep --colour"

# Disable correction.
alias ack='nocorrect ack'
alias cd='nocorrect cd'
alias cp='nocorrect cp'
alias ebuild='nocorrect ebuild'
alias gcc='nocorrect gcc'
alias gist='nocorrect gist'
alias grep='nocorrect grep'
alias heroku='nocorrect heroku'
alias ln='nocorrect ln'
alias man='nocorrect man'
alias mkdir='nocorrect mkdir'
alias mv='nocorrect mv'
alias mysql='nocorrect mysql'
alias rm='nocorrect rm'
alias sudo='nocorrect sudo '

# Disable globbing.
alias bower='noglob bower'
alias fc='noglob fc'
alias find='noglob find'
alias ftp='noglob ftp'
alias history='noglob history'
alias locate='noglob locate'
alias rake='noglob rake'
alias rsync='noglob rsync'
alias scp='noglob scp'
alias sftp='noglob sftp'

#DIRS
alias -g ...='../../'           #cd ...
alias -g ....='../../../'       #cd ....
alias -g .....='../../../../'   #cd .....
