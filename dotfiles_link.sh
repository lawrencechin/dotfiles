#!/bin/bash

# Backup selected dotfiles
LinkFiles=( "config/nvim" "tmux-macos" "tmux-other" "tmux.conf" "config/mc/ini" "zsh/zshrc" "zsh/zlogin" "bashrc" "bash_profile" "config/exercism/user.json" "config/fish" "config/kitty")

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Functions
checkForSlashes() {
    echo "$1" | grep \/ -c
}

linkFiles() {
    rm -r "$2"
    ln -sf "$1" "$2"
}
splitPath() {
    # create path needed to make dirs and the file name to link
    # directory=$( echo $1 | sed -E 's/((.+\/)+)(.+)$/\1/g' )
    directory=$( dirname "$1" ) 
    mkdir -p "$HOME/.$directory"
    linkFiles "$DIR/$1" "$HOME/.$1"
}
init() {
    if test "$1" = "zsh/zshrc" -o "$1" = "zsh/zlogin"; then
        linkFiles "$DIR/$1" "$HOME"/."$( basename "$1" )"
    elif ! (( "$(checkForSlashes "$1")" )); then
        linkFiles "$DIR/$1" "$HOME/.$1"
    else
        splitPath "$1"
    fi
}

for dotfiles in "${LinkFiles[@]}"; do
        init "$dotfiles"
done
